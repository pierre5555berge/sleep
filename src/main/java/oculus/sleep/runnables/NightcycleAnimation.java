package oculus.sleep.runnables;

import oculus.sleep.Sleep;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

public class NightcycleAnimation extends BukkitRunnable {

    private final World world;

    public NightcycleAnimation(World world) {
        this.world = world;
    }

    public void run() {
        if (!Sleep.isNight(world)) {
            cancel();
        } else {
            world.setTime(world.getTime() + 85);
        }
    }

}
